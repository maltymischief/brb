#!/usr/bin/env python3

import socket
import threading
import AudioPlayer


class ThreadedServer(object):
	def __init__(self, audioplayer, host='00:1B:DC:06:18:C8', port=24):
		
		#self.AP = AudioPlayer.AudioPlayer("sussudio.mp3")#move outside class and pass in.
		self.AP = audioplayer
	
		self.host = host
		self.port = port
		print(self.host, self.port)
		self.sock = socket.socket(socket.AF_BLUETOOTH, \
			socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)
		self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.sock.bind((self.host, self.port))

	def listen(self):
		self.sock.listen(5)
		while True:
			client, address = self.sock.accept()
			client.settimeout(60)
			threading.Thread(target = self.listenToClient,args = (client,address)).start()

	def listenToClient(self, client, address):
		size = 1024
		print("CONNECTED", address)
		while True:
			try:
				data = client.recv(size)
				if data:
					print(str(data))
					
					if str(data) == "b\'WELLLOOOOOME!\'":
						self.AP.play(play_millis=3000, fade_millis=3000)
				
					# Set the response to echo back the recieved data 
					#print(type(data))
					mesg = "Playing song"
					client.send(mesg.encode('utf-8'))
				else:
					raise error('Client disconnected')
			except:
				print("DISCONNECTED", address)
				client.close()
				return False


if __name__ == "__main__":
	AP = AudioPlayer.AudioPlayer("sussudio.mp3")#move outside class and pass in.
	ThreadedServer(AP).listen()








#!/usr/bin/env python3

"""
"""
from pygame import mixer,time


class AudioPlayer():
    def __init__(self, audiofile):
        
        mixer.pre_init(44100, -16, 2, 3072)
        mixer.init()
        mixer.music.set_volume(1.0)
        
        self.audiofile = audiofile
        mixer.music.load(self.audiofile)
        
    def play(self, play_millis=3000, fade_millis=5000):
        print("PUSHUP TIME")
        mixer.music.play(loops=0, start=0.0)
        if time.delay(play_millis):
            print("time's up")
            mixer.music.fadeout(fade_millis)
            mixer.music.unload()
            mixer.music.load(self.audiofile)

    

if __name__ == "__main__":
	AP = AudioPlayer("/home/malty/sussudio.mp3")
	AP.play()

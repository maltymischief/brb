#!/usr/bin python3


"""
A simple Python script to receive messages from a client over
Bluetooth using Python sockets (with Python 3.3 or above).
"""
from pygame import mixer,time
import socket

#adjust playback buffer
mixer.pre_init(44100, -16, 2, 3072)
mixer.init()

#hostMACAddress = '5C:F3:70:93:C8:28' # The MAC address of a Bluetooth adapter on the server. The server might have multiple Bluetooth adapters.
hostMACAddress = '00:1B:DC:06:18:C8' # The MAC address of a Bluetooth adapter on the server. The server might have multiple Bluetooth adapters. ##pi
port = 24 # 3 is an arbitrary choice. However, it must match the port used by the client.
backlog = 1
size = 1024
s = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)
s.bind((hostMACAddress,port))

s.listen(backlog)

mixer.music.load("/home/malty/sussudio.mp3")
mixer.music.set_volume(1.0)






try:
    client, address = s.accept()
    print(address)
    while 1:
        data = client.recv(size)
        if data:
            print(str(data))
            
            if str(data) == "b\'s\'":
                print("PUSHUP TIME")
                mixer.music.play(loops=0, start=0.0)
                if time.delay(3000):
                    print("time's up")
                    mixer.music.fadeout(5000)
                mixer.music.unload()
                mixer.music.load("/home/malty/sussudio.mp3")
            client.send(data)
except:	
    print("Lost client")	
    #client.close()
    #s.close()


const int ACTIVATION_MIN_INTERVAL = 660;  //minutes
const int ACTIVATION_MAX_INTERVAL = 660;  //minutes
const long BLINK_INTERVAL = 300;           // interval at which to blink (milliseconds)

const int buttonPin = 8;    // the number of the pushbutton pin
const int lowPin = A15;   //pulldown pin --> gnd
const int ledPin = 13;      // the number of the LED pin

// Variables will change:
int buttonState;             // the current reading from the input pin
int ledState = HIGH;         // the current state of the output pin
int lastButtonState = LOW;   // the previous reading from the input pin

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 1;    // the debounce time; increase if the output flickers
long previousMillis = 0;        // will store last time LED was updated


void setup() {
  Serial.begin(9600);
  pinMode(lowPin, OUTPUT);
  digitalWrite(lowPin, LOW);
  pinMode(buttonPin, INPUT_PULLUP);
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, ledState);
}


void loop() {
  // read the state of the switch into a local variable:
  int reading = digitalRead(buttonPin);
  //Serial.println(digitalRead(buttonPin));
  unsigned long currentMillis = millis();

  /**
     Blinking Logic*******************************
  */
  if (currentMillis - previousMillis > BLINK_INTERVAL) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;
    // if the LED is off turn it on and vice-versa:
    if (ledState == LOW)
      ledState = HIGH;
    else
      ledState = LOW;

    // set the LED with the ledState of the variable:
    digitalWrite(ledPin, ledState);


    /**
      Button Debouncing Logic**********************************
    **/
    if (reading != lastButtonState) {
      // reset the debouncing timer
      lastDebounceTime = millis();
    }
    if ((millis() - lastDebounceTime) > debounceDelay) {
      if (reading != buttonState) {
        buttonState = reading;
        /*********************************************************
        */

        if (buttonState == LOW) { //enter when button has been pressed
          int randNumber = random(ACTIVATION_MIN_INTERVAL, ACTIVATION_MAX_INTERVAL);
          Serial.println(randNumber);
          freeze(randNumber); //delay for 1 second
        }
      }
    }
  }
  digitalWrite(ledPin, ledState);

  // save the reading. Next time through the loop, it'll be the lastButtonState:
  lastButtonState = reading;
}


/**
   Disables button and light for n minutes as "interval" arg
*/
void freeze(float interval) {
  digitalWrite(ledPin, LOW);
  for (int i = 0; i < interval; i++) { //n_minutes
    for (int i = 0; i <= 60; ++i) { //minute
      delay(1000);
    }
  }
  digitalWrite(ledPin, HIGH);
}

# Import socket module 
import socket 


def Main():
    hostMACAddress = '00:1B:DC:06:18:C8'
    port = 24
 
    s = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)
    s.connect((hostMACAddress,port))
    
    # message you send to server 
    message = "WELLLOOOOOME!"
    while True:
        # message sent to server 
        s.send(message.encode('ascii')) 

	# messaga received from server 
        data = s.recv(1024) 

	# print the received message 
	# here it would be a reverse of sent message 
        print('Received from the server :',str(data.decode('ascii'))) 

	# ask the client whether he wants to continue 
        ans = input('\nDo you want to continue(y/n) :') 
        if ans == 'y':
            continue
        else:
            break
        # close the connection
        s.close() 

if __name__ == '__main__':
    Main() 

